material-design-icon 需要用户翻墙才能看到图标,否则就只有文字. 因而需要本地的图标  
具体查看: [material-design-icons](http://google.github.io/material-design-icons/)

## 安装本地的 material-design-icon
```shell
npm install material-design-icons
```

## 引入字体文件
```css
/* https://fonts.googleapis.com/icon?family=Material+Icons */
/* fallback */
@font-face {
	font-family: 'Material Icons';
	font-style: normal;
	font-weight: 400;
	src: url('../fonts/material1.woff2') format('woff2'),
		url('../fonts/material2.woff2') format('woff2'),
		url('../fonts/material3.woff2') format('woff2');
	/* 上面的三个文件是下面的三个文件的本地重命名后的文件, 如果使用原名, 还是会从一下路径去下载, 而导致失败 */
		/* src: url(https://fonts.gstatic.com/s/roboto/v18/KFOlCnqEu92Fr1MmEU9fBBc4.woff2) format('woff2'),
	url(https://fonts.gstatic.com/s/materialicons/v36/flUhRq6tzZclQEJ-Vdg-IuiaDsNc.woff2) format('woff2'),
	url(https://fonts.gstatic.com/s/roboto/v18/KFOmCnqEu92Fr1Mu4mxK.woff2) format('woff2'); */
}
  
.material-icons {
	font-family: 'Material Icons';
	font-weight: normal;
	font-style: normal;
	font-size: 24px;
	line-height: 1;
	letter-spacing: normal;
	text-transform: none;
	display: inline-block;
	white-space: nowrap;
	word-wrap: normal;
	direction: ltr;
	-webkit-font-feature-settings: 'liga';
	-webkit-font-smoothing: antialiased;
}
```