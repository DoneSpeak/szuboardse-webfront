import Vue from 'vue'
import Router from 'vue-router'
import Index from '@/pages/Index'
import NotFound from '@/pages/NotFound'
import UserProfile from '@/pages/UserProfile'
import Search from '@/pages/Search'
import Error from '@/pages/Error'
import Security from '@/components/SecuritySettings'
import UserInfo from '@/components/UserInfo'
import Favorite from '@/components/Favorite'
import FavoriteArticles from '@/components/FavoriteArticles'
import FavoriteAttaches from '@/components/FavoriteAttaches'
import Info from '@/components/Info'

import $store from '../store/store'
import bus from '../util/bus'
import tokenUtil from "../util/token"
import userAPI from '../api/user'
import APIHelper from '../api/helper'

Vue.use(Router)

let router = new Router({
  routes: [
    {
      path: '/',
      name: 'Index',
      component: Index
    },
    {
      path: '/user',
      name: 'UserProfile',
      component: UserProfile,
      children: [
        {
          path: "",
          component: UserInfo
        },
        {
          path: "profile",
          component: UserInfo
        },
        {
          path: "security",
          component: Security
        },
        {
          path: "favorite/articles",
          component: FavoriteArticles
        },
        {
          path: 'favorite/attaches',
          component: FavoriteAttaches
        },
        {
          path: "info",
          component: Info
        }
      ]
    },
    {
      path: '/search',
      name: 'Search',
      component: Search
    },
    {
      path: 'error',
      name: 'Error',
      component: Error
    },
    {
      path: '*',
      name: 'NotFound',
      component: NotFound
    }
  ]
})

/**
  {
    "jti": "744b9145-30d5-4466-9c05-2190c862e5aa",
    "iat": 1523454872,
    "sub": "4",
    "exp": 1526046872
  }`
 * 如果token过期，且未失效，则从后台刷新token，并返回 true。
 * 如果token失效，则直接返回false.
 * @param {*} token 
 */
function tokenValid(token) {
  const INVALID = 0;
  const VALID = 1;
  const WAIT = 2;
  if(!token || token == 'undefined') {
    return INVALID;
  }
  let claim = tokenUtil.parseToken(token);
  let now = new Date().getTime() / 1000;
  // let refreshTimeSpan = 10 * 60;
  let refreshTimeSpan = 7 * 24 * 60 * 60;
  let timespan = claim.exp - now;
  if(timespan > 0) {
    // 还没有失效
    if(timespan <= refreshTimeSpan) {
      // 需要刷新 token
      userAPI.refreshToken((err, res) => {
        if (err) {
          // TODO@DoneSpeak 服务器端的错误直接作为 失效
          return WAIT;
        }
        if(!APIHelper.isSuccessResult(res.code)) {
          return WAIT;
        }
        $store.refreshToken(res.data.token);
        return VALID;
      });
    }
  } else {
    return INVALID;
  }
  return VALID;
}

router.beforeEach(async (to, from, next) => {
  let token = window.localStorage.getItem("token");
  let isLogin = !!token;
  switch(to.path){
    case '/':
    case '/search': {
      if(isLogin){
        if(!tokenValid(token)) {
          $store.cleanUserInfo();
          const callback = {};
          bus.$emit("User-Signout", callback);
        }
      }
      next();
      break;
    }
    default: {
      if(to.name == 'NotFound' || to.name == 'Error') {
        next();
        break;
      }
      // 这些路径都需要登录
      let isTokenValid = tokenValid(token);
      if(isLogin && isTokenValid){
        next();
      } else {
        $store.cleanUserInfo();
        const callback = {};
        bus.$emit("User-Signout", callback);
        next({'name':'Index'});
      }
      break;
    }
  }
});

export default router;
