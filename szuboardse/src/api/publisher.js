import Vue from "vue";
import store from "../store/store";

export default class PublisherAPI {
  static listPublishers(callback) {
    console.log('没问题')
    const url = store.domain + '/publishers';
    Vue.http.get(url).then(
      res => {
        callback(null, res.data);
      },
      err => {
        callback(err);
      }
    );
  }
}