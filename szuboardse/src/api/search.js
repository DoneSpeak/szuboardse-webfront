import Vue from 'vue'
import store from "../store/store"

export default class SearchAPI {
  static search(options, callback) {
    
    let url = store.domain + '/search'
    if(!!options) {
      url += '?';
      if(!!options.query) {
        url += 'query=' + options.query + '&';
      }
      if(!!options.region) {
        url += 'region=' + options.region + '&';
      }
      if(!!options.category) {
        url += 'category=' + options.category + '&';
      }
      if(!!options.publisher) {
        url += 'publisher=' + options.publisher + '&';
      }
      if(!!options.durationEnd) {
        url += 'durationEnd=' + options.durationEnd + '&';
      }
      if(!!options.durationStart) {
        url += 'durationStart=' + options.durationStart + '&'
      }
      if(!!options.pageIndex) {
        url += 'pageIndex=' + options.pageIndex;
      } else {
        url += 'pageIndex=1'
      }
    }
    Vue.http.get(url).then((res) => {
      callback(null, res.data)
    }, err => {
      callback(err)
    })
  }

  static relativeQuery(query, callback) {
    const url = store.domain + '/query/relative?query=' + query + '&top=' + 15;
    Vue.http.get(url).then((res) => {
      callback(null, res.data)
    }, err => {
      callback(err)
    })
  }
}