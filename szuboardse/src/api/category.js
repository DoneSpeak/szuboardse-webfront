import Vue from "vue";
import store from "../store/store";

export default class CategoryAPI {
  static listCategories(callback) {
    const url = store.domain + '/categories';
    Vue.http.get(url).then(
      res => {
      console.log('刚刚返回');
        callback(null, res.data);
      },
      err => {
        callback(err);
      }
    );
  }
}