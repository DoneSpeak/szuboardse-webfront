import Vue from 'vue'
import store from "../store/store"

export default class UserAPI {
  static signin(user, callback) {
    const url = store.domain + '/tokens';
    let email = user.email;
    let password = user.password;
    Vue.http.post(url, {
      email, password
    }).then((res) => {
      callback(null, res.data)
    }, err => {
      callback(err)
    })
  }

  static refreshToken(callback) {
    const url = store.domain + '/tokens';
    Vue.http.put(url).then((res) => {
      callback(null, res.data)
    }, err => {
      callback(err)
    })
  }

  static signup(user, callback) {
    const url = store.domain+'/users';
    let email = user.email;
    let password = user.password;
    Vue.http.post(url, {
      email, password
    }).then((res) => {
      callback(null, res.data)
    }, err => {
      callback(err)
    })
  }

  static signout(uid, callback) {
    const url = store.domain+'/users/' + uid + "/connection"
    Vue.http.delete(url).then((res) => {
      callback(null, res.data)
    }, err => {
      callback(err)
    })
  }

  static updateName(uid, name, callback) {
    const url = store.domain + '/users/' + uid + '/name';
    
    Vue.http.patch(url, {
      uid, name
    }).then((res) => {
      callback(null, res.data)
    }, err => {
      callback(err)
    })
  }

  static updatePassword(uid, email, password, newPassword, callback) {
    const url = store.domain + '/users/' + uid + '/password';
    
    Vue.http.patch(url, {
      uid, email, password, newPassword
    }).then((res) => {
      callback(null, res.data)
    }, err => {
      callback(err)
    })
  }
}