import Vue from "vue";
import store from "../store/store";

// reference: [vue http请求 delete传值问题](https://segmentfault.com/q/1010000007948714)
// reference: [Attaching data (body) to $http.delete event in VueJS](https://stackoverflow.com/questions/39916939/attaching-data-body-to-http-delete-event-in-vuejs)
export default class FavoriteAPI {
  static listFavoriteSrc(uid, callback) {
    const url = store.domain + '/users/' + uid + '/favorite/sources';
    Vue.http.get(url).then(
      res => {
        callback(null, res.data);
      },
      err => {
        callback(err);
      }
    );
  }

  static addFavoriteSrc(params, callback) {
    let uid = params.uid;
    let categoryId = params.categoryId;
    let publisherId = params.publisherId;
    const url = store.domain + '/users/' + uid + '/favorite/sources';
    Vue.http.post(url, {
        categoryId, publisherId
    }).then(
      res => {
        callback(null, res.data);
      },
      err => {
        callback(err);
      }
    );
  }

  static deleteFavoriteSrc(uid, sourceId, callback) {
    const url = store.domain + '/users/' + uid + '/favorite/sources/' + sourceId;
    Vue.http.delete(url).then(
      res => {
        callback(null, res.data);
      },
      err => {
        callback(err);
      }
    );
  }

  static removeFavoriteSrc(uid, categoryId, publisherId, callback) {
    let params = {uid, categoryId, publisherId};

    const url = store.domain + '/users/' + uid + '/favorite/sources';
    Vue.http.delete(url, { 
      body: {categoryId, publisherId}
      }).then(
        res => {
          callback(null, res.data);
        },
        err => {
          callback(err);
        }
      );
  }

  static listFavoriteArticles(uid, pageIndex, pageSize, callback) {
    const url = store.domain + '/users/' + uid + '/favorite/articles?pageIndex='
      + pageIndex + '&pageSize=' + pageSize;
    Vue.http.get(url).then(
        res => {
          callback(null, res.data);
        },
        err => {
          callback(err);
        }
      );
  }

  static listFavoriteAttaches(uid, pageIndex, pageSize, callback) {
    const url = store.domain + '/users/' + uid + '/favorite/attaches?pageIndex='
      + pageIndex + '&pageSize=' + pageSize;
    Vue.http.get(url).then(
        res => {
          callback(null, res.data);
        },
        err => {
          callback(err);
        }
      );
  }


  static addFavoriteArticle(uid, id, callback) {
    const url = store.domain + '/users/' + uid + '/favorite/articles';
    Vue.http.post(url, {
        id
    }).then(
      res => {
        callback(null, res.data);
      },
      err => {
        callback(err);
      }
    );
  }

  static addFavoriteAttach(uid, id, callback) {
    const url = store.domain + '/users/' + uid + '/favorite/attaches';
    Vue.http.post(url, {
        id
    }).then(
      res => {
        callback(null, res.data);
      },
      err => {
        callback(err);
      }
    );
  }

  static removeFavoriteArticle(uid, id, callback) {
    const url = store.domain + '/users/' + uid + '/favorite/articles/' + id;
    Vue.http.delete(url).then(
      res => {
        callback(null, res.data);
      },
      err => {
        callback(err);
      }
    );
  }

  static removeFavoriteAttach(uid, id, callback) {

    const url = store.domain + '/users/' + uid + '/favorite/attaches/' + id;
    Vue.http.delete(url).then(
      res => {
        callback(null, res.data);
      },
      err => {
        callback(err);
      }
    );
  }

}