// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import VueResource from 'vue-resource'
import Vuetify from 'vuetify'

import $store from './store/store'
// import 'element-ui/lib/theme-chalk/index.css'

import 'vuetify/dist/vuetify.min.css' // Ensure you are using css-loader
import 'material-design-icons' // Ensure you are using css-loader
import './assets/css/global.css'

Vue.config.productionTip = false

Vue.use(VueResource)
Vue.use(Vuetify, {
  theme: {
    primary: '#C2185B',
    secondary: '#009688',
    accent: '#82B1FF',
    error: '#FF5252',
    info: '#2196F3',
    success: '#4CAF50',
    warning: '#FFC107'
  }
})

Vue.http.interceptors.push((request, next)  =>{
  // 请求之前的处理
  //登录成功后将后台返回的TOKEN在本地存下来,每次请求从sessionStorage中拿到存储的TOKEN值  
  let token = $store.getTokenFromStorage();
  
  if(token){
    //如果请求时TOKEN存在,就为每次请求的headers中设置好TOKEN,后台根据headers中的TOKEN判断是否放行  
    request.headers.set('x-access-token', token);  
  }
  // 请求之后的处理  
  next((response) => {
    // 没有访问权限
    let currentRouter = router.history.current.name;
    if(response.status == 403) {
      // 清空本地缓存
      $store.cleanUserInfo();
      // 回到主页
      console.log(currentRouter);
      if(currenRouter == 'Index' || currenRouter != 'Search') {
        router.replace({
          name: 'Index',
          query: {redirect: router.currentRoute.fullPath}
        });
      }
    }
    return response;
  });
})

// 初始化存储，主要是从 localstorage 中初始化用户登陆信息
$store.init();

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
