export default {
  formatDate(format){
    var date = {
      'M+': this.getMonth() + 1,
      'd+': this.getDate(),
      'h+': this.getHours(),
      'm+': this.getMinutes(),
      's+': this.getSeconds(),
      'q+': Math.floor((this.getMonth() + 3) / 3),
      'S+': this.getMilliseconds()
    };
    if (/(y+)/i.test(format)) {
      format = format.replace(RegExp.$1, (this.getFullYear() + '').substr(4 - RegExp.$1.length));
    }
    for (var k in date) {
      if (new RegExp('(' + k + ')').test(format)) {
        format = format.replace(RegExp.$1, RegExp.$1.length == 1
            ? date[k] : ('00' + date[k]).substr(('' + date[k]).length));
      }
    }
    return format;
  },
  addData(date, number, interval) {
    let targetDate = new Date(date.toDateString());
    switch (interval) {
    case 'y': {
        targetDate.setFullYear(date.getFullYear() + number);
        return targetDate;
    }
    case 'q': {
        targetDate.setMonth(date.getMonth() + number * 3);
        return targetDate;
    }
    case 'm': {
        targetDate.setMonth(date.getMonth() + number);
        return targetDate;
    }
    case 'w': {
        targetDate.setDate(date.getDate() + number * 7);
        return targetDate;
    }
    case 'd': {
        targetDate.setDate(date.getDate() + number);
        return targetDate;
    }
    case 'h': {
        targetDate.setHours(date.getHours() + number);
        return targetDate;
    }
    case 'm': {
        targetDate.setMinutes(date.getMinutes() + number);
        return targetDate;
    }
    case 's': {
        targetDate.setSeconds(date.getSeconds() + number);
        return targetDate;
    }
    default: {
        targetDate.setDate(date.getDate() + number);
        return targetDate;
    }
    }
  },
  getDayBegin() {
    let now = new Date();
    now.setHours(0);
    now.setMinutes(0);
    now.setSeconds(0);
    return now;
  },
  getDayEnd() {
    let now = new Date();
    now.setHours(23);
    now.setMinutes(59);
    now.setSeconds(59);
    return now;
  },

  /**
   * 
   * @param
   * reference:
   * [6 very basic but very useful JavaScript Number Format Functions for web developers]
   *    (http://ntt.cc/2008/04/25/6-very-basic-but-very-useful-javascript-number-format-functions-for-web-developers.html)} value 
   * @param {*} decimal 
   */
  formatNumber (value, decimal) {
    //decimal  - the number of decimals after the digit from 0 to 3
    //-- Returns the passed number as a string in the xxx,xxx.xx format.
    let anynum = eval(value);
    let divider = 10;
    switch(decimal){
      case 0:
        divider =1;
        break;
      case 1:
        divider =10;
        break;
      case 2:
        divider =100;
        break;
      default:       //for 3 decimal places
        divider =1000;
    } 

    let workNum = Math.abs((Math.round(anynum * divider) / divider)); 

    let workStr = "" + workNum 

    if (workStr.indexOf(".") == -1){
      workStr += ".";
    } 

    let dStr = workStr.substr(0, workStr.indexOf("."));
    let dNum = dStr-0;
    let pStr = workStr.substr(workStr.indexOf(".")) 

    while (pStr.length - 1 < decimal){
      pStr+="0";
    } 

    if(pStr =='.'){
      pStr = '';
    }

    //--- Adds a comma in the thousands place.
    let dLen = 0;
    if (dNum >= 1000) {
      dLen = dStr.length
      dStr = parseInt("" + (dNum / 1000)) + "," + dStr.substring(dLen-3, dLen);
    } 

    //-- Adds a comma in the millions place.
    if (dNum >= 1000000) {
      dLen = dStr.length
      dStr = parseInt("" + (dNum / 1000000)) + "," + dStr.substring(dLen-7, dLen)
    }
    let retval = dStr + pStr
    //-- Put numbers in parentheses if negative.
    if (anynum < 0){
      retval = "(" + retval + ")";
    }

    //You could include a dollar sign in the return value.
    //retval =  "$"+retval
    return retval;
  }
}