export default {
  parseToken(token) {
    if(!token) {
      return null;
    }
    let payload = token.split('.')[1];
    let payloadJson = window.atob(payload);
    let tokenData = JSON.parse(payloadJson);
    return tokenData;
  }
}