export default {
  // domain: 'http://localhost:8080/szuboardse-web',
  // domain: 'http://192.168.1.176:8080/szuboardse-web',
  domain: 'http://carelative.donespeak.cn/resources',
  auth: {},
  user: {
    name: '',
    email: '',
    avatar: '',
    uid: '',
    isLogin: false
  },
  init() {
    this.getTokenFromStorage();
    this.getUserFromStorage();
  },
  storeUser(user, token) {
    this.user = user;
    window.localStorage.setItem("user", JSON.stringify(this.user));
    window.localStorage.setItem("token", token);
  },
  isLogin() {
    return !!window.localStorage.getItem("token");
  },
  getTokenFromStorage() {
    return window.localStorage.getItem("token");
  },
  getUserFromStorage() {
    if(!this.isLogin()) {
      return this.createEmptyUser();
    }
    let user = JSON.parse(window.localStorage.getItem("user"));
    user.isLogin = true;
    this.user = user;
    return user;
  },
  cleanUserInfo() {
    window.localStorage.removeItem("token");
    window.localStorage.removeItem("user");
    this.user = this.createEmptyUser();
  },
  updateUser(user) {
    this.user = user;
    window.localStorage.setItem('user', JSON.stringify(this.user));
  },
  refreshToken(token) {
    window.localStorage.setItem("token", token);
  },
  createEmptyUser() {
    return {
      avatar: '',
      name: '',
      email: '',
      uid: '',
      isLogin: false
    }
  }
}