export default {  
  categories: [
    {
      id: 1,
      name: '学术'
    },
    {
      id: 2,
      name: '教务'
    },
    {
      id: 3,
      name: '行政'
    },
    {
      id: 4,
      name: '学工'
    },
    {
      id: 5,
      name: '校园'
    }
  ],
  publishers: [
    {
      id: 1,
      name: '计算机与软件学院'
    },
    {
      id: 2,
      name: '外国语学院'
    },
    {
      id: 3,
      name: '艺术设计学院'
    },
  ],
  relativeQueries: [
    '这是相关s检索',
    '这是也是相关a的a检索',
    '相关的检索啦s啦啦了',
    '这是也是相关的检d索',
    '相关的检索啦啦d啦了',
    '这是也是相关的s检索',
    '相关的检索啦啦s啦s了',
    '这是也是相关a的检索',
    '相关的检索d啦啦啦了',
    '这是也是相关的e检索',
    '相关的检索啦w啦e啦了',
    '这a是也是相关的检索',
    '相关的检索啦啦e啦了',
    'lalaland',
    '15',
    '16',
    '17',
    '18'
  ],
  resultItems: [
    {
      type: 'ARTICLE',
      id: 1000,
      articleId: 10000,
      title: '关于预计2018届本科毕结业学生核对学历电子图像及补采集的通知',
      url: 'http://www1.szu.edu.cn/board/view.asp?id=363874',
      category: {
          id: 1,
          name: '教务'
      },
      publisher: {
          id: '54',
          name: '计算机与软件学院'
      },
      publishedTime: 1521731068128,
      updatedTime: 1521731068129,
      digest: '根据上级主管部门的安排，广东省2018届<span class=\'szuboardse-highlight\'>毕业生</span>电子图像信息补采集工作将于2018年4月30日结束。我校2018届本科毕结业学生已参加集中采集的学历证书电子图像的核对及应补采集等工作安排如下：一、已参加集中采集的学生务必分别到以下网址核对本人学历注册电子图像信息',
      isfavorite: true,
      isArticle: true,
      isAttach: false,
      pictures: [     // 文章特有
        {
          id:155,
          articleId: 10000,
          path: 'http://olbpic.ol-img.com/album/201305/02/14324433lvnck5645b41wb.jpg',  // 本地服务器的路径
          url: 'http://www1.szu.edu.cn/images/uooc.png',   // 图片来源的路径
        }
      ],
      attaches: [      // 文章特有
        {
          id: 55,
          filename: '2018预计毕业生未参加学历图像采集人数统计_20180323.xls',
          url: 'http://www1.szu.edu.cn/board/uploadfiles/2018326302018%E9%A2%84%E8%AE%A1%E6%AF%95%E4%B8%9A%E7%94%9F%E6%9C%AA%E5%8F%82%E5%8A%A0%E5%AD%A6%E5%8E%86%E5%9B%BE%E5%83%8F%E9%87%87%E9%9B%86%E4%BA%BA%E6%95%B0%E7%BB%9F%E8%AE%A1_20180323.xls'
        },
        {
          id: 56,
          filename: '2018预计毕业生为采集学历图像名单364人_20180323.xls',
          url: 'http://www1.szu.edu.cn/board/uploadfiles/2018326382018%E9%A2%84%E8%AE%A1%E6%AF%95%E4%B8%9A%E7%94%9F%E4%B8%BA%E9%87%87%E9%9B%86%E5%AD%A6%E5%8E%86%E5%9B%BE%E5%83%8F%E5%90%8D%E5%8D%95364%E4%BA%BA_20180323.xls'
        }
      ]
    },
    {
      type: 'ATTACH',
      id: 55,
      articleId: 55,
      title: '2018<span class=\'szuboardse-highlight\'>预计毕业生</span>未参加学历图像采集人数统计_20180323.xls',
      url: 'http://www1.szu.edu.cn/board/uploadfiles/2018326302018%E9%A2%84%E8%AE%A1%E6%AF%95%E4%B8%9A%E7%94%9F%E6%9C%AA%E5%8F%82%E5%8A%A0%E5%AD%A6%E5%8E%86%E5%9B%BE%E5%83%8F%E9%87%87%E9%9B%86%E4%BA%BA%E6%95%B0%E7%BB%9F%E8%AE%A1_20180323.xls',
      category: {
          id: 1,
          name: '教务'
      },
      pictures: [],
      publisher: {
          id: '54',
          name: '计算机与软件学院'
      },
      publishedTime: 1521731068128,
      updatedTime: 1521731068128,
      digest: '经我部统计核对，截止到2018年3月23日，<span class=\'szuboardse-highlight\'>在预计2018年毕结业</span>的6596名本科生中，尚有364人未参加学历证书电子图像采集（详见EHall及附件）。还未参加学历证书电子图像采集的同学务必于2018年4月30日前到广州市越秀区府前路21号二楼205进行最后补拍，联系电话：020-83302077。',
      isfavorite: false,
      isArticle: false,
      isAttach: true,
      attaches: [
        {
          id: 56,
          filename: '2018预计毕业生为采集学历图像名单364人_20180323.xls',
          url: 'http://www1.szu.edu.cn/board/uploadfiles/2018326382018%E9%A2%84%E8%AE%A1%E6%AF%95%E4%B8%9A%E7%94%9F%E4%B8%BA%E9%87%87%E9%9B%86%E5%AD%A6%E5%8E%86%E5%9B%BE%E5%83%8F%E5%90%8D%E5%8D%95364%E4%BA%BA_20180323.xls'
        }
      ]
    }
  ],
  pageInfo: {
    pageIndex: 1,
    pageSize: 10,
    pageNum: 45,
    total: 456,
  },
  subInfo: {
    timecost: 12133,
    totalhits: 2
  },
  favorites: [
    {
      category: {
        id: 1,
        name: '学术'
      },
      publishers: [
        {
          id: 63,
          name: "术在我心",
        },
        {
          id: 27,
          name: "走在时代的前面"
        }
      ]
    },
    {
      category: {
        id: 2,
        name: '教务'
      },
      publishers: [
        {
          id: 64,
          name: "不停息的鸟",
        },
        {
          id: 132,
          name: "美食总动员"
        },
        {
          id: 122,
          name: "带你走天南地北"
        },
        {
          id: 77,
          name: "美食打卡"
        },
        {
          id: 2,
          name: "英语狂人"
        },
        {
          id: 37,
          name: "今晚打老虎"
        }
      ]
    },
    {
      category: {
        id: 3,
        name: '行政'
      },
      publishers: [
        {
          id: 66,
          name: "滑稽社",
        },
        {
          id: 19,
          name: "深大奇幻圈"
        }
      ]
    },
    {
      category: {
        id: 4,
        name: '学工'
      },
      publishers: [
        {
          id: 57,
          name: "科学怪咖",
        },
        {
          id: 103,
          name: "外国语学院"
        }
      ]
    },
    {
      category: {
        id: 5,
        name: '校园'
      },
      publishers: [
        {
          id: 72,
          name: "素描大院",
        },
        {
          id: 111,
          name: "油画大家"
        }
      ]
    }
  ],
  generateResultItems() {
    let items = [];
    let url = this.resultItems[0].url;
    for(let i = 0; i < 5; i ++) {
      items[i] = this.resultItems[i % 2];
      items[i].id = i;
      items[i].url = url + '#' + i;
    }
    return items;
  },
  generatePublisher() {
    let publishers = [];
    let charSet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    for(let idx = 0; idx < 150; idx ++) {
      var randomString = '';
      for (var i = 0; i < 15; i++) {
        var randomPoz = Math.floor(Math.random() * charSet.length);
        randomString += charSet.substring(randomPoz,randomPoz+1);
      }
      let last = publishers.length;
      publishers[last] = {};
      publishers[last].id = idx;
      publishers[last].name = randomString;
    }
    return publishers;
  }
}